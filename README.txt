This project contains Drupal customization hooks for use with Fedora
Insight.  They are not guaranteed to work with other installations.

All contents are licensed GPLv2+ except where noted otherwise.
